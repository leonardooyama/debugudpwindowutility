#include "widget.h"
#include "ui_widget.h"
#include <QStyle>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QIntValidator>
#include <QDateTime>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QHostAddress>
#include <QDebug>
#include <QNetworkDatagram>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    // icons for buttoms
    QStyle *style = QApplication::style();
    ui->pushButton_StartListening->setIcon(style->standardIcon(QStyle::SP_MediaPlay, 0, this));
    ui->pushButton_StopListening->setIcon(style->standardIcon(QStyle::SP_MediaStop, 0, this));
    ui->pushButton_ClearLog->setIcon(style->standardIcon(QStyle::SP_DialogResetButton, 0, this));
    ui->pushButton_SaveLog->setIcon(style->standardIcon(QStyle::SP_DialogSaveButton, 0, this));

    // validators for line edit widgets
    QRegularExpression re("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");
    QRegularExpressionValidator *val = new QRegularExpressionValidator(re,this);
    val->setRegularExpression(re);
    ui->lineEdit_HostName->setValidator(val);
    ui->lineEdit_PortNumber->setValidator(new QIntValidator(0, 65535, this));

    // initial values for line edits
    ui->lineEdit_HostName->setText("255.255.255.255");
    ui->lineEdit_PortNumber->setText("10000");

    // connection between the push buttoms and the functions that handle the click signal
    connect(ui->pushButton_StartListening, &QPushButton::clicked, this, &Widget::handlePushButtomStartListening);
    connect(ui->pushButton_StopListening, &QPushButton::clicked, this, &Widget::handlePushButtomStopListening);
    connect(ui->pushButton_ClearLog, &QPushButton::clicked, this, &Widget::handlePushButtomClearLog);
    connect(ui->pushButton_SaveLog, &QPushButton::clicked, this, &Widget::handlePushButtomSaveLog);

    // initial setup for the log widget
    ui->textEdit_UDPLog->setReadOnly(true);
    QString startupMsg = "Log cleared at " + getCurrentFomatedDateTime() + "\n";
    ui->textEdit_UDPLog->append(startupMsg);

    // initial setup for the line push buttom's state
    ui->pushButton_StartListening->setFocus();
    ui->pushButton_StartListening->setDefault(true);
    ui->pushButton_StopListening->setEnabled(false);

    // initial setup for the udp socket
    udpSocketListener = new QUdpSocket(this);
    connect(udpSocketListener, &QUdpSocket::readyRead, this, &Widget::readPendingDatagrams);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,
                                  "Quit",
                                  "Do you really want to quit?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        QMessageBox::StandardButton reply2;
        reply2 = QMessageBox::question(this,
                                      "Save log",
                                      "Do you want to save the log before leaving?",
                                      QMessageBox::Yes|QMessageBox::No);
        if (reply2 == QMessageBox::Yes)
        {
            handlePushButtomSaveLog(true);
        }
        event->accept();
    }
    if (reply == QMessageBox::No)
    {
        event->ignore();
    }
}

QString Widget::getCurrentFomatedDateTime()
{
    return QDateTime::currentDateTime().toString("dd-MMM-yyyy, hh'h' mm'min' ss's'");
}

void Widget::handlePushButtomStartListening(bool clicked)
{
    QHostAddress hostAdd(ui->lineEdit_HostName->text());
    if (ui->lineEdit_HostName->text() == "255.255.255.255")
    {
        hostAdd = QHostAddress::AnyIPv4;
    }
    qint16 portNumber = ui->lineEdit_PortNumber->text().toUInt();
    if (!udpSocketListener->bind(QHostAddress::AnyIPv4, portNumber, QAbstractSocket::ReuseAddressHint))
    {
        ui->textEdit_UDPLog->append("Failed to bind host IP and port.");
        return;
    }

    QString startListeningMsg = "Started listening to the host " + ui->lineEdit_HostName->text() +
            ":" + ui->lineEdit_PortNumber->text() + " at " + getCurrentFomatedDateTime() + "\n";
    ui->textEdit_UDPLog->append(startListeningMsg);

    ui->pushButton_StartListening->setEnabled(false);
    ui->pushButton_StopListening->setEnabled(true);
    ui->pushButton_StopListening->setFocus();
    ui->pushButton_StopListening->setDefault(true);
}

void Widget::handlePushButtomStopListening(bool clicked)
{
    udpSocketListener->close();
    QString stopListeningMsg = "Stoped listening to the host " + ui->lineEdit_HostName->text() +
            ":" + ui->lineEdit_PortNumber->text() + " at " + getCurrentFomatedDateTime() + "\n";
    ui->textEdit_UDPLog->append(stopListeningMsg);
    ui->pushButton_StopListening->setEnabled(false);
    ui->pushButton_StartListening->setEnabled(true);
    ui->pushButton_StartListening->setFocus();
    ui->pushButton_StartListening->setDefault(true);
}

void Widget::handlePushButtomClearLog(bool clicked)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,
                                  "Clear Log",
                                  "Do you really want to clear the log?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        ui->textEdit_UDPLog->clear();
        QString startupMsg = "Log cleared at " + getCurrentFomatedDateTime() + "\n";
        ui->textEdit_UDPLog->append(startupMsg);
    }
    if (reply == QMessageBox::No)
    {
        qDebug() << "Do not clear the log!";
    }
}

void Widget::handlePushButtomSaveLog(bool clicked)
{
    QString suggestedFileName = QDateTime::currentDateTime().toString("yyyy-MM-dd_hh'h'-mm'm'-ss's'");
    suggestedFileName.append(".log");
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save log file"),
                                                    suggestedFileName,
                                                    tr("log (*.log)"));
    if (fileName.isEmpty())
    {
        QMessageBox::information(this, "Log file NOT saved", "The log file was not saved.");
        return;
    }
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, "Error saving log file", "Log file not saved");
        return;
    }
    QTextStream out(&file);
    out << ui->textEdit_UDPLog->toPlainText();
    file.close();
    QMessageBox::information(this, "Log file saved", "Log file successfully saved!");
}

void Widget::readPendingDatagrams()
{
    while (udpSocketListener->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = udpSocketListener->receiveDatagram();
        QString datragramInfo;
        datragramInfo = "Read date-time: " + getCurrentFomatedDateTime() + "\n";
        datragramInfo+= "Sender address: " + datagram.senderAddress().toString() + "\n";
        datragramInfo+= "Sender port: " + QString::number(datagram.senderPort()) + "\n";
        datragramInfo+= "Destination address: " + datagram.destinationAddress().toString() + "\n";
        datragramInfo+= "Destination port: " + QString::number(datagram.destinationPort());
        QString data = "Datagram data:\n" + QString::fromUtf8(datagram.data()) + "\n";
        ui->textEdit_UDPLog->append(datragramInfo);
        ui->textEdit_UDPLog->append(data);
        data.clear();
        datagram.clear();
    }
}
